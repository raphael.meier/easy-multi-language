<?php

/*
 
Plugin Name: Easy Multi Language
 
Description: A simple an easy Javascript plugin which manage your multi language.
 
Version: 0.0.1

Author: Raphaël Meier
 
Author URI: https://raphael-meier.com

Text Domain: easy-multi-language
 
*/

require_once(plugin_dir_path(__FILE__) . 'includes/settings.php');

if (!function_exists('easy_multi_language_enqueue_scripts_and_styles')) {
    function easy_multi_language_enqueue_scripts_and_styles()
    {
        if (!is_admin()) {
            wp_enqueue_script('easy_multi_language_translator_script', plugin_dir_url(__FILE__) . 'assets/js/translator.js', array('jquery'));
            wp_enqueue_style('easy_multi_language_translator_style', plugin_dir_url(__FILE__) . 'assets/css/translator.css');
            wp_enqueue_script('easy_multi_language_lang_script', plugin_dir_url(__FILE__) . 'assets/js/lang.js');
            wp_enqueue_style('easy_multi_language_lang_style', plugin_dir_url(__FILE__) . 'assets/css/lang.css');
        }
    }
}
add_action('wp_enqueue_scripts', 'easy_multi_language_enqueue_scripts_and_styles');

function easy_multi_language_enqueue_settings_script()
{
    wp_enqueue_script('easy_multi_language_settings_script', plugin_dir_url(__FILE__) . 'assets/js/settings.js');
    wp_enqueue_style('easy_multi_language_settings_style', plugin_dir_url(__FILE__) . 'assets/css/settings.css');
}
add_action('admin_enqueue_scripts', 'easy_multi_language_enqueue_settings_script');

function callback($buffer)
{
    // modify buffer here, and then return the updated code
    $start = strpos($buffer, '</header>');

    $options = get_option('easy_multi_language_options');

    $html_lang = '<select id="easy-multi-language-languages" onchange=switchLang(this)>';

    foreach ($options as $key => $option) {
        $common_root_length = strlen(COMMON_ROOT);
        if (substr($key, 0, $common_root_length) == COMMON_ROOT) {
            $language = substr($key, $common_root_length);
            if ($_COOKIE["lang"] == $language) {
                $html_lang .= '<option class="easy-multi-language-lang" value="' . $language . '") selected>' . $language . '</option>';
            } else {
                $html_lang .= '<option class="easy-multi-language-lang" value="' . $language . '")>' . $language . '</option>';
            }
        }
    }

    $html_lang .= "</select>";
    $buffer = substr_replace($buffer, $html_lang, $start, 0);
    return $buffer;
}
function buffer_start()
{
    ob_start("callback");
}
add_action('wp_head', 'buffer_start');


require_once(plugin_dir_path(__FILE__) . 'includes/get_dictionary.php');
