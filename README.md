# Easy Multi Language

It is a Multi Language WordPress plugin. Do not use it, it is just a POC.

The point is to do the translation entirely in Javascript once the page is loaded. There is some major issues.

The user can have two languages for now: english and french.

## How to use it

Every where you want a translation write the key inside the translation tag: **{{KEY}}**.

The json files are like this

```(json)
{

    "FIRST_KEY": "KEY1FR",
    "SECOND_KEY": "KEY2FR",
    "THIRD_KEY": "KEY3FR"

}
```

## TODO

- When dictionary is empty (or not find), switch back on the default dictionary
- Verify the impact on the SEO.
- Maybe switch to a PHP translator.
