const COMMON_ROOT = "easy_multi_language_dictionary";

const easyMultiLanguageAddDictionary = () => {
  const add_dictionary_button = document.getElementById(
    "easy_multi_language_add_dictionary_button"
  );

  const new_dictionary_div = document.createElement("div");
  new_dictionary_div.className = "easy_multi_language_dictionary_entry";

  const new_dictionary_input = document.createElement("input");
  new_dictionary_input.type = "text";
  new_dictionary_input.onchange = () => {
    dictionaryInputOnChange(new_dictionary_input);
  };
  new_dictionary_div.appendChild(new_dictionary_input);

  const new_dictionary_textarea = document.createElement("textarea");
  new_dictionary_textarea.rows = 4;
  new_dictionary_textarea.cols = 50;
  new_dictionary_textarea.id = COMMON_ROOT + "_";
  new_dictionary_textarea.name = `easy_multi_language_options[${COMMON_ROOT}]`;

  new_dictionary_div.appendChild(new_dictionary_textarea);

  const new_dictionary_delete_button = document.createElement("div");
  new_dictionary_delete_button.className =
    "easy_multi_language_delete_dictionary_entry_button";
  new_dictionary_delete_button.innerText = "Delete dictionary";
  new_dictionary_delete_button.onclick = () => {
    deleteDictionaryEntry(new_dictionary_delete_button);
  };

  new_dictionary_div.appendChild(new_dictionary_delete_button);

  add_dictionary_button.parentNode.insertBefore(
    new_dictionary_div,
    add_dictionary_button
  );
};

const dictionaryInputOnChange = (input_html_element) => {
  dictionary_textarea =
    input_html_element.parentNode.getElementsByTagName("textarea")[0];
  if (dictionary_textarea) {
    dictionary_textarea.id =
      dictionary_textarea.id.substring(0, COMMON_ROOT.length + 1) +
      input_html_element.value;

    dictionary_textarea.name = `easy_multi_language_options[${COMMON_ROOT}_${input_html_element.value}]`;
  }
};

const deleteDictionaryEntry = (delete_button_html_element) => {
  delete_button_html_element.parentNode.remove();
};
