const OPENING_TAG = "{{";
const CLOSING_TAG = "}}";
const DEFAULT_LANG = "fr";

const getUserLang = () => {
  const lang_position = document.cookie.indexOf("lang");
  if (lang_position === -1) {
    return DEFAULT_LANG;
  } else {
    const lang_ending_position = document.cookie.indexOf(
      ";",
      lang_position + 5
    );
    if (lang_ending_position === -1) {
      return document.cookie.substring(
        lang_position + 5,
        document.cookie.length
      );
    } else {
      return document.cookie.substring(lang_position + 5, lang_ending_position);
    }
  }
};

const replaceKeys = (source, dictionary) => {
  let closing_tag_position = 0;
  let opening_tag_position = 0;

  while (
    (opening_tag_position = source.indexOf(
      OPENING_TAG,
      closing_tag_position + CLOSING_TAG.length
    )) !== -1
  ) {
    // Extract the key
    closing_tag_position = source.indexOf(
      CLOSING_TAG,
      opening_tag_position + OPENING_TAG.length
    );

    if (closing_tag_position === -1) {
      break;
    }

    key = source.substring(
      opening_tag_position + OPENING_TAG.length,
      closing_tag_position
    );

    source =
      source.substring(0, opening_tag_position) +
      dictionary[key] +
      source.substring(closing_tag_position + CLOSING_TAG.length);
  }

  return source;
};

const translate = (lang) => {
  jQuery
    .getJSON(
      `${window.location.href}?feed=get_easy_multi_language_dictionary&&lang=${lang}`,
      (dictionary) => {
        document.body.innerHTML = replaceKeys(
          document.body.innerHTML,
          dictionary
        );
        document.documentElement.style.visibility = "visible";
      }
    )
    .fail((jqXHR, textStatus, errorThrown) => {
      console.log("Error when getting json: ", errorThrown);
      document.documentElement.style.visibility = "visible";
    });
};

document.addEventListener("DOMContentLoaded", () => {
  const lang = getUserLang();
  translate(lang);
});
