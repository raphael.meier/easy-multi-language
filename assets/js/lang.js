const switchLang = (select) => {
  const selected_option = select.querySelector("option:checked");
  console.log(selected_option);
  if (selected_option) {
    document.cookie = `lang=${selected_option.value}`;
    location.reload();
  }
};
