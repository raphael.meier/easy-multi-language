<?php
/* Dictionary endpoint */
if (!function_exists('easy_multi_language_render_dictionary')) {
    function easy_multi_language_render_dictionary()
    {
        header('Content-Type: application/json');
        if (isset($_GET["lang"])) {
            $options = get_option('easy_multi_language_options');
            $dictionary = $options["easy_multi_language_dictionary_" . $_GET["lang"]];
            if ($dictionary) {
                echo $dictionary;
            } else {
                echo "{}";
            }
        } else {
            $options = get_option('easy_multi_language_options');
            $dictionary = $options["easy_multi_language_dictionary_en"];
            echo $dictionary;
        }
    }
}

if (!function_exists('easy_multi_language_add_dictionnaries_feed')) {
    function easy_multi_language_add_dictionnaries_feed()
    {
        add_feed('get_easy_multi_language_dictionary', 'easy_multi_language_render_dictionary');
    }
}
add_action('init', 'easy_multi_language_add_dictionnaries_feed');
