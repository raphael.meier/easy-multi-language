<?php

define("COMMON_ROOT", "easy_multi_language_dictionary_");

/**
 * custom option and settings
 */
function easy_multi_language_settings_init()
{
    // Register a new setting for "easy_multi_language" page.
    register_setting('easy_multi_language', 'easy_multi_language_options');

    // Register a new section in the "easy_multi_language" page.
    add_settings_section(
        'easy_multi_language_section_dictionaries',
        __('Dictionaries', 'easy_multi_language'),
        'easy_multi_language_section_dictionaries_callback',
        'easy_multi_language'
    );
}

/**
 * Register our easy_multi_language_settings_init to the admin_init action hook.
 */
add_action('admin_init', 'easy_multi_language_settings_init');


/**
 * Custom option and settings:
 *  - callback functions
 */

function easy_multi_language_section_dictionaries_callback($args)
{
?>
    <p id="<?php echo esc_attr($args['id']); ?>"><?php esc_html_e('Put your JSON dictionaries here! Be careful of JSON syntax error!', 'easy_multi_language'); ?></p>
<?php
}

/**
 * Add the top level menu page.
 */
function easy_multi_language_options_page()
{
    add_menu_page(
        'easy_multi_language',
        'Easy Multi Language',
        'manage_options',
        'easy_multi_language',
        'easy_multi_language_options_page_html'
    );
}


/**
 * Register our easy_multi_language_options_page to the admin_menu action hook.
 */
add_action('admin_menu', 'easy_multi_language_options_page');


/**
 * Top level menu callback function
 */
function easy_multi_language_options_page_html()
{
    // check user capabilities
    if (!current_user_can('manage_options')) {
        return;
    }

    // add error/update messages

    // check if the user have submitted the settings
    // WordPress will add the "settings-updated" $_GET parameter to the url
    if (isset($_GET['settings-updated'])) {
        // add settings saved message with the class of "updated"
        add_settings_error('easy_multi_language_messages', 'easy_multi_language_message', __('Settings Saved', 'easy_multi_language'), 'updated');
    }

    // show error/update messages
    settings_errors('easy_multi_language_messages');
?>
    <div class="wrap">
        <h1><?php echo esc_html(get_admin_page_title()); ?></h1>
        <form action="options.php" method="post">
            <?php
            settings_fields('easy_multi_language');
            do_settings_sections('easy_multi_language');
            ?>
            <?php
            $options = get_option('easy_multi_language_options');

            foreach ($options as $key => $option) {
                $common_root_length = strlen(COMMON_ROOT);
                if (substr($key, 0, $common_root_length) == COMMON_ROOT) {
                    $language = substr($key, $common_root_length);
            ?>
                    <div class="easy_multi_language_dictionary_entry">
                        <input type='text' value='<?php echo $language; ?>' onchange=dictionaryInputOnChange(this)></input>
                        <textarea rows="4" cols="50" id="<?php echo COMMON_ROOT . $language; ?>" name="easy_multi_language_options[<?php echo COMMON_ROOT . $language; ?>]"><?php echo $option ?></textarea>
                        <div class="easy_multi_language_delete_dictionary_entry_button" onclick=deleteDictionaryEntry(this)>Delete dictionary</div>
                    </div>
            <?php
                }
            }
            ?>

            <input type="button" onclick=easyMultiLanguageAddDictionary() value="Add dictionary" id="easy_multi_language_add_dictionary_button" class="button button-primary"></input>
            <?php
            submit_button('Save Settings');
            ?>
        </form>
    </div>
<?php
}
